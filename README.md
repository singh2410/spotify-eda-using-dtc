# Spotify EDA using DTC
## By-Aarush Kumar
Exploratory Data Analysis of Spotify data using DecisionTreeClassifier.
Exploratory data analysis (EDA) is used by data scientists to analyze and investigate data sets and summarize their main characteristics, often employing data visualization methods. It helps determine how best to manipulate data sources to get the answers you need, making it easier for data scientists to discover patterns, spot anomalies, test a hypothesis, or check assumptions.
EDA is primarily used to see what data can reveal beyond the formal modeling or hypothesis testing task and provides a provides a better understanding of data set variables and the relationships between them. It can also help determine if the statistical techniques you are considering for data analysis are appropriate.
Some glimps of data visualization-![Screenshot_from_2021-05-25_03-43-04](/uploads/d80b14375bfb2ec63308afb01803c445/Screenshot_from_2021-05-25_03-43-04.png)
The final Decision DecisionTreeClassifier-
![Screenshot_from_2021-05-25_03-43-50](/uploads/65b5585b73cc442d01328b5d331e74da/Screenshot_from_2021-05-25_03-43-50.png)
Thankyou!
