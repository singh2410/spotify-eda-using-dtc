#!/usr/bin/env python
# coding: utf-8

# # Spotify EDA on songs dataset.
# #By- Aarush Kumar
# #Dated: May 24,2021

# In[93]:


get_ipython().system('pip install pydotplus')
get_ipython().system('pip install graphviz')
get_ipython().system('pip install scipy')


# In[94]:


import pandas as pd
import numpy as np
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
import io
from io import StringIO
from sklearn.tree import DecisionTreeClassifier, export_graphviz
import pydotplus
from pydotplus import graph_from_dot_data
import graphviz
import matplotlib.pyplot


# In[95]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/SpotifyEDA/data.csv')
df


# In[96]:


type(df)


# In[97]:


df.describe()


# In[98]:


df.head()


# In[99]:


df.info()


# In[100]:


df.isnull().sum()


# In[101]:


train, test=train_test_split(df, test_size=0.15)


# In[102]:


print('Training size:{} :Test size:{}'.format(len(train), len(test)))


# In[103]:


train.shape


# In[104]:


test.shape


# In[105]:


red_blue=["#1985FE","#EF4836"]
palette=sns.color_palette(red_blue)
sns.set_palette(palette)
sns.set_style("white")


# In[106]:


pos_tempo=df[df['target']==1]['tempo']
neg_tempo=df[df['target']==0]['tempo']

pos_dance=df[df['target']==1]['danceability']
neg_dance=df[df['target']==0]['danceability']
pos_duration=df[df['target']==1]['duration_ms']
neg_duration=df[df['target']==0]['duration_ms']
pos_loudness=df[df['target']==1]['loudness']
neg_loudness=df[df['target']==0]['loudness']
pos_speechiness=df[df['target']==1]['speechiness']
neg_speechiness=df[df['target']==0]['speechiness']
pos_valence=df[df['target']==1]['valence']
neg_valence=df[df['target']==0]['valence']
pos_energy=df[df['target']==1]['energy']
neg_energy=df[df['target']==0]['energy']
pos_acousticness=df[df['target']==1]['acousticness']
neg_acousticness=df[df['target']==0]['acousticness']
pos_key=df[df['target']==1]['key']
neg_key=df[df['target']==0]['key']
pos_instrumentalness=df[df['target']==1]['instrumentalness']
neg_instrumentalness=df[df['target']==0]['instrumentalness']


# In[107]:


fig=plt.figure(figsize=(12,8))
plt.title("Song Tempo Liked vs Disliked")
pos_tempo.hist(alpha=0.7, bins=30, label='positive')
neg_tempo.hist(alpha=0.7, bins=30, label='negative')
plt.legend(loc='upper right')


# In[108]:


fig, axs = plt.subplots(3, 3, figsize=(15, 15))
#Danceability
axs[0, 0].hist(pos_dance, alpha=0.5, bins=30)
axs[0, 0].hist(neg_dance, alpha=0.5, bins=30)
axs[0, 0].set_xlabel('Danceability')
axs[0, 0].set_ylabel('Count')
axs[0, 0].set_title('Song Danceability Like/Dislike Distribution')

#Duration
axs[0, 1].hist(pos_duration, alpha=0.5, bins=30)
axs[0, 1].hist(neg_duration, alpha=0.5, bins=30)
axs[0, 1].set_xlabel('Duration')
axs[0, 1].set_ylabel('Count')
axs[0, 1].set_title('Song Duration Like/Dislike Distribution')

#Loudness
axs[0, 2].hist(pos_loudness, alpha=0.5, bins=30)
axs[0, 2].hist(neg_loudness, alpha=0.5, bins=30)
axs[0, 2].set_xlabel('Loudness')
axs[0, 2].set_ylabel('Count')
axs[0, 2].set_title('Song Loudness Like/Dislike Distribution')

#Speechiness
axs[1, 0].hist(pos_speechiness, alpha=0.5, bins=30)
axs[1, 0].hist(neg_speechiness, alpha=0.5, bins=30)
axs[1, 0].set_xlabel('Speechiness')
axs[1, 0].set_ylabel('Count')
axs[1, 0].set_title('Song Speechiness Like/Dislike Distribution')

#Valence
axs[1, 1].hist(pos_valence, alpha=0.5, bins=30)
axs[1, 1].hist(neg_valence, alpha=0.5, bins=30)
axs[1, 1].set_xlabel('Valence')
axs[1, 1].set_ylabel('Count')
axs[1, 1].set_title('Song Valence Like/Dislike Distribution')

#Energy
axs[1, 2].hist(pos_energy, alpha=0.5, bins=30)
axs[1, 2].hist(neg_energy, alpha=0.5, bins=30)
axs[1, 2].set_xlabel('Energy')
axs[1, 2].set_ylabel('Count')
axs[1, 2].set_title('Song Energy Like/Dislike Distribution')

#Acousticness
axs[2, 0].hist(pos_acousticness, alpha=0.5, bins=30)
axs[2, 0].hist(neg_acousticness, alpha=0.5, bins=30)
axs[2, 0].set_xlabel('Acousticness')
axs[2, 0].set_ylabel('Count')
axs[2, 0].set_title('Song Acousticness Like/Dislike Distribution')

#Key
axs[2, 1].hist(pos_key, alpha=0.5, bins=30)
axs[2, 1].hist(neg_key, alpha=0.5, bins=30)
axs[2, 1].set_xlabel('Key')
axs[2, 1].set_ylabel('Count')
axs[2, 1].set_title('Song Key Like/Dislike Distribution')

#Instrumentalness
axs[2, 2].hist(pos_instrumentalness, alpha=0.5, bins=30)
axs[2, 2].hist(neg_instrumentalness, alpha=0.5, bins=30)
axs[2, 2].set_xlabel('Instrumentalness')
axs[2, 2].set_ylabel('Count')
axs[2, 2].set_title('Song Instrumentalness Like/Dislike Distribution')


# In[126]:


c=DecisionTreeClassifier(min_samples_split=100)


# In[127]:


features=['danceability','loudness','valence','energy','instrumentalness','acousticness','key','speechiness','duration_ms']


# In[128]:


X_train=train[features]
Y_train=train['target']

X_test=test[features]
Y_test=test['target']


# In[129]:


Y_test


# In[130]:


dt=c.fit(X_train,Y_train)


# In[131]:


def show_tree(tree,features,path):
    f=io.StringIO()
    export_graphviz(tree, out_file=f, feature_names=features)
    pydotplus.graph_from_dot_data(f.getvalue()).write_png(path)
    img=matplotlib.pyplot.imread(path)
    plt.rcParams['figure.figsize']=(20,20)
    plt.imshow(img)


# In[132]:


show_tree(dt, features, 'dec_tree_01.png')


# In[133]:


Y_pred=c.predict(X_test)


# In[134]:


Y_pred


# In[135]:


from sklearn.metrics import accuracy_score
score=accuracy_score(Y_test, Y_pred)*100


# In[136]:


print("Accuarcy score is: ",round(score, 1), "%")

